[![unsafe forbidden](https://img.shields.io/badge/unsafe-forbidden-success.svg)](https://github.com/rust-secure-code/safety-dance/)
[![License](https://img.shields.io/badge/license-MIT-blue.svg)](https://raw.githubusercontent.com/onur/cargo-license/master/LICENSE)

# libfancontrold
Libfancontrold is the base library for fancontrold, a system daemon written in rust that controls your computer's fans depending on various configurable sensor inputs.

## Usage

Just add this to your `Cargo.toml` file:

```toml
[dependencies]
libfancontrold = "0.3"
```

## Cargo-Features

* `async`: Build the asynchronous version of this library.

## Configuration
Libfancontrold supports configuration in json format.

The configuration must have the following structure:

```
{
    "channels": [channel objects]
}
```

Each channel object needs to have the following fields:

* "hwmon_device_path": The path to the to be controlled fan's hwmon device. "/sys/devices/platform/nct6775.2592" or something similar.
* "index": The index of the controlled fan.
* "temps": An array of temperature objects to derive the fan speed from. There must be at least one temperature object in this array or a path in "virtual_temps". Each temperature object needs to have the following fields:
    * "hwmon_device_path": The path to the temps's hwmon device.
    * "index": The index of the temp sensor.
* "virtual_temps": An array of file paths to derive the fan speed from. There must be at least one path in this array or one temperature object in "temps".
* "curve": An Object with pairs of temp and pwm values that form a curve like this:
```
"Curve": {
    "40": 50,
    "50": 100,
    "60": 200
}
```

Optional fields for channel objects:

* "mtf": If there are multiple objects in the `temps` array, this field determines the way which one of the temps will be checked to calculate the fan speed.
There are three possible values:
    * "Min" - The lowest temperature is chosen.
    * "Max" - The highest temperature is chosen.
    * "Average" - An average of all temperatures is calculated.
* "min_pwm": (0-255) Minimum pwm value for this channel. Defaults to 100.
* "min_start": (0-255) Pwm value to restart this fan if it stopped. Only used if `min_pwm` is greater than zero.
* "pwm_mode": The mode of fan speed regulation. It can have the following values:
    * "PWM" - Pulse width modulation. Requires four pin connector.
    * "DC" - Voltage control. Should only be used with three pin fans.
    * "Automatic" - Uses PWM if motherboard connector supports it, DC otherwise. Recommended and default value.
* "average": (1-10) Number of passed seconds to calculate an average temperature from. Can be used to smooth temperature spikes over. Defaults to 1.
