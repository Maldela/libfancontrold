use crate::address::Error as AddressError;

use std::{
    error::Error as StdError,
    fmt::{Display, Formatter, Result as FmtResult},
    io::Error as IoError,
    path::PathBuf,
};

use serde_json::error::Error as SerdeError;

pub type Result<T> = std::result::Result<T, Error>;

#[derive(Debug)]
pub enum Error {
    EmptyTemps,
    EmptyCurve,
    Address { source: AddressError },
    Index,
    Average,
    Load { path: PathBuf, source: IoError },
    Parse { source: SerdeError },
}

impl Error {
    pub fn new_load(path: impl Into<PathBuf>, source: IoError) -> Self {
        Error::Load {
            path: path.into(),
            source,
        }
    }
}

impl Display for Error {
    fn fmt(&self, f: &mut Formatter<'_>) -> FmtResult {
        match self {
            Error::EmptyTemps => write!(f, "Config has no temps."),
            Error::EmptyCurve => write!(f, "Config has an empty curve."),
            Error::Address { source } => write!(f, "Config has invalid sensor address: {}", source),
            Error::Index => write!(f, "Config has no index or index is not above zero."),
            Error::Average => write!(f, "Config has an average below one or above 10."),
            Error::Load { path, source } => {
                write!(f, "Error loading config at {}: {}", path.display(), source)
            }
            Error::Parse { source } => write!(f, "Error parsing config: {}", source),
        }
    }
}

impl StdError for Error {
    fn source(&self) -> Option<&(dyn StdError + 'static)> {
        match self {
            Error::EmptyTemps => None,
            Error::EmptyCurve => None,
            Error::Address { source } => Some(source),
            Error::Index => None,
            Error::Average => None,
            Error::Load { source, .. } => Some(source),
            Error::Parse { source } => Some(source),
        }
    }
}

impl From<SerdeError> for Error {
    fn from(source: SerdeError) -> Self {
        Error::Parse { source }
    }
}

impl From<AddressError> for Error {
    fn from(source: AddressError) -> Self {
        Error::Address { source }
    }
}
