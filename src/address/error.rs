use std::{
    error::Error as StdError,
    fmt::{Display, Formatter, Result as FmtResult},
    path::PathBuf,
};

use super::Address;

pub type Result<T> = std::result::Result<T, Error>;

#[derive(Debug)]
pub enum Error {
    Hwmon { device_path: PathBuf },
    Fan { address: Address },
    Pwm { address: Address },
    Temp { address: Address },
}

impl Error {
    pub fn hwmon_not_found(device_path: impl Into<PathBuf>) -> Self {
        Error::Hwmon {
            device_path: device_path.into(),
        }
    }
    pub fn fan_not_found(address: impl Into<Address>) -> Self {
        Error::Fan {
            address: address.into(),
        }
    }
    pub fn pwm_not_found(address: impl Into<Address>) -> Self {
        Error::Pwm {
            address: address.into(),
        }
    }
    pub fn temp_not_found(address: impl Into<Address>) -> Self {
        Error::Temp {
            address: address.into(),
        }
    }
}

impl Display for Error {
    fn fmt(&self, f: &mut Formatter<'_>) -> FmtResult {
        match self {
            Error::Hwmon { device_path } => {
                write!(f, "Could not find hwmon: {}", device_path.display())
            }
            Error::Fan { address } => write!(f, "Could not find fan {}.", address),
            Error::Pwm { address } => write!(f, "Could not find pwm {}.", address),
            Error::Temp { address } => write!(f, "Could not find temp {}.", address),
        }
    }
}

impl StdError for Error {
    fn source(&self) -> Option<&(dyn StdError + 'static)> {
        None
    }
}
